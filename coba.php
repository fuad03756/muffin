<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>php</title>
</head>
<body>

<?php 
	function createProduct($data) {
	  // Masukkan data produk ke database
	  $sql = "INSERT INTO products (name, price, description) VALUES (?, ?, ?)";
	  $stmt = $pdo->prepare($sql);
	  $stmt->execute([
	    $data['name'],
	    $data['price'],
	    $data['description'],
	  ]);

	  // Kembalikan ID produk baru
	  return $pdo->lastInsertId();
	}

	function readProducts() {
	  // Ambil semua data produk dari database
	  $sql = "SELECT * FROM products";
	  $stmt = $pdo->prepare($sql);
	  $stmt->execute();

	  // Kembalikan array produk
	  return $stmt->fetchAll();
	}

	function readProduct($id) {
	  // Ambil data produk dari database berdasarkan ID
	  $sql = "SELECT * FROM products WHERE id = ?";
	  $stmt = $pdo->prepare($sql);
	  $stmt->execute([$id]);

	  // Kembalikan produk
	  return $stmt->fetch();
	}

	function updateProduct($id, $data) {
	  // Perbarui data produk di database
	  $sql = "UPDATE products SET name = ?, price = ?, description = ? WHERE id = ?";
	  $stmt = $pdo->prepare($sql);
	  $stmt->execute([
	    $data['name'],
	    $data['price'],
	    $data['description'],
	    $id,
	  ]);

	  // Kembalikan true jika berhasil
	  return $stmt->rowCount() > 0;
	}

	function deleteProduct($id) {
	  // Hapus data produk dari database
	  $sql = "DELETE FROM products WHERE id = ?";
	  $stmt = $pdo->prepare($sql);
	  $stmt->execute([$id]);

	  // Kembalikan true jika berhasil
	  return $stmt->rowCount() > 0;
	}
 ?>

</body>
</html>